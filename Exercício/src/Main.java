public class Main {

    public static void main(String[] args) {
        Wardrobe Wardrobe = new Wardrobe();

        Sports shirt = new Sports();
        shirt.setColor("Blue");
        shirt.setDescription("tank top");
        shirt.setNumber(40);
        shirt.setPrice(37.3f);
        shirt.setFabric("polyester");

        Social suit = new Social();
        suit.setColor("Grey");
        suit.setDescription("Royal blue");
        suit.setNumber(44);

        Social blouse = new Social();
        blouse.setColor("Green");
        blouse.setDescription("No clip");
        blouse.setNumber(40);
        blouse.setPrice(55.9f);
        blouse.setFabric("wool");

        Work bata = new Work();
        bata.setColor("Blue");
        bata.setDescription("Black wooden");
        blouse.setNumber(42);
        bata.setPrice(70.9f);
        bata.setFabric("cotton");
        
        

        Wardrobe.addClothes(Wardrobe.rightDoor, shirt);
        Wardrobe.addClothes(Wardrobe.leftDoor, suit);
        Wardrobe.addClothes(Wardrobe.leftDoor, blouse);
        Wardrobe.addClothes(Wardrobe.rightDrawer, bata);

        Wardrobe.printClothes(Wardrobe.rightDoor);
        Wardrobe.printClothes(Wardrobe.leftDoor);
        Wardrobe.printClothes(Wardrobe.rightDrawer);
        Wardrobe.printClothes(Wardrobe.leftDrawer);

        System.out.println("Predominant color: " + Wardrobe.preDominantColor());
    }
}
