public class Formal extends Clothes {
    @Override
    public void fold() {
        System.out.println("Formal clothes folded.");
    }

    @Override
    public void wash() {
        System.out.println("Formal clothes washed.");
    }

    @Override
    public String toString() {
        return "Formal: " + super.toString();
    }
}