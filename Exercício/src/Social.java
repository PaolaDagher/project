public class Social extends Clothes {
    @Override
    public void fold() {
        System.out.println("Social clothes folded.");
    }

    @Override
    public void wash() {
        System.out.println("Social clothes washed.");
    }

    @Override
    public String toString() {
        return "Social: " + super.toString();
    }
}