public class Work extends Clothes {
    @Override
    public void fold() {
        System.out.println("Work clothes folded.");
    }

    @Override
    public void wash() {
        System.out.println("Work clothes washed.");
    }

    @Override
    public String toString() {
        return "Work: " + super.toString();
    }
}