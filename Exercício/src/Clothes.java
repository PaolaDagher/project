public class Clothes {

    private String description;
    private String color;
    private String status;
    private int number;
    private float price;
    private String fabric;

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getColor() {
        return color;
    }

    public void setColor(String color) {
        this.color = color;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public int getNumber() {
        return number;
    }

    public void setNumber(int number) {
        this.number = number;
    }
    public int getPrice() {
    	return number;
    }
    
    public void setPrice(float price) {
    	this.price = price;
    }
    
    
    public String getFabric() {
    	return fabric;
    }
    
    public void setFabric(String fabric) {
    	this.fabric = fabric;
    }
    
    public void fold() {
        System.out.println("Folded Clothing");
    }
    public void wash() {
        System.out.println("Washed Clothing");
    }

    @Override
    public String toString() {
        return String.format("%s %s %s %s %s %s", this.description, this.color, this.status, this.number, this.price, this.fabric);
    }
}