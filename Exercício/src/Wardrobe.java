import java.util.ArrayList;
import java.util.Collections;

public class Wardrobe {

    public ArrayList<Clothes> rightDoor;
    public ArrayList<Clothes> leftDoor;
    public ArrayList<Clothes> rightDrawer;
    public ArrayList<Clothes> leftDrawer;

    public Wardrobe() {
        this.rightDoor = new ArrayList<Clothes>();
        this.leftDoor = new ArrayList<Clothes>();
        this.rightDrawer = new ArrayList<Clothes>();
        this.leftDrawer = new ArrayList<Clothes>();
    }

    public void addClothes(ArrayList<Clothes> compartment, Clothes r) {
        compartment.add(r);
    }

    public void removeClothes(ArrayList<Clothes> compartment, Clothes r) {
        compartment.remove(r);
    }

    public void printClothes(ArrayList<Clothes> compartment) {
        for (Clothes r : compartment) {
            System.out.println(r);
        }
    }

    public int totalClothesInWardrobe() {
        return rightDoor.size() + leftDoor.size() + rightDrawer.size() + leftDrawer.size();
    }

    public String preDominantColor() {
        ArrayList<String> allColor = new ArrayList<>();
        // adding all the color clothes in the list
        for (Clothes clothes : leftDoor)
            allColor.add(clothes.getColor());
        for (Clothes clothes : leftDrawer)
            allColor.add(clothes.getColor());
        for (Clothes clothes : rightDoor)
            allColor.add(clothes.getColor());
        for (Clothes clothes : rightDrawer)
            allColor.add(clothes.getColor());

        // sorting the list of the basis of color;
        Collections.sort(allColor);
        String preDominantColor = "NONE", tempColor = allColor.get(0);
        int maxCount = 1, tempCount = 1;
        for (int i = 1; i < allColor.size(); i++) {
            if (tempColor.equalsIgnoreCase(allColor.get(i)))
                tempCount++;
            else {
                if (tempCount > maxCount) {
                    maxCount = tempCount;
                    preDominantColor = tempColor;
                }
                tempCount = 1;
                tempColor = allColor.get(i);
            }
        }
        return preDominantColor;
    }
}