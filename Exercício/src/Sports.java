public class Sports extends Clothes {
    @Override
    public void fold() {
        System.out.println("Sports clothes folded.");
    }

    @Override
    public void wash() {
        System.out.println("Sports clothes washed.");
    }

    @Override
    public String toString() {
        return "Sports: " + super.toString();
    }
}